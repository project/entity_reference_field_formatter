<?php

namespace Drupal\entity_reference_field_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\system\Entity\Menu;

/**
 * Plugin implementation of the 'entity_reference_url_title' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_url_title",
 *   label = @Translation("Entity Url,Title"),
 *   description = @Translation("Entity reference formatter that separates entity url & title."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceUrlTitleFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'separator' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['separator'] = [
      '#title' => $this->t('Separator'),
      '#type' => 'textfield',
      '#description' => $this->t('Separator character used for title and url values.'),
      '#size' => 10,
      '#default_value' => $this->getSetting('separator'),
      '#required' => TRUE,
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->getSetting('separator') ? $this->t('Separator : %separator', ['%separator' => $this->getSetting('separator')]) : $this->t('No Separator');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    global $base_url;
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      // @todo Handle different entity instances?
      if ($entity instanceof Menu) {
        $internal_path = NULL;
      }
      elseif ($entity instanceof Node) {
        $uri = $entity->toUrl();
        $internal_path = $uri->getInternalPath();
      }
      else {
        $uri = $entity->urlInfo();
        $internal_path = $uri->getInternalPath();
      }

      $label = $entity->label();
      $elements[$delta] = [
        '#plain_text' => $label . $this->getSetting('separator') . $base_url . '/' . $internal_path,
      ];
    }

    return $elements;
  }

}
