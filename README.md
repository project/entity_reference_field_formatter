## Entity Reference Field Formatter

Entity Reference formatter provides new formatter for the entity
reference field. This formatter is used in web services to pass
"title" and "url" separately. Title and URL can be separated by
a separator character.

### Requirements

No special requirements at this time.

### Install

* Install this module.
* Go to Manage display of the entity reference field.
* You can see the new formatter called "Entity Url,Title", select
it and provide a separator character in the settings.

### Maintainers

Current Maintainers:
* George Anderson

  https://www.drupal.org/u/geoanders

* Saranya Purushothaman

  https://www.drupal.org/u/saranya-purushothaman
